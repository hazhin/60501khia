<?php namespace App\Controllers;

use App\Models\PlayerModel;
use App\Models\TeamModel;
use CodeIgniter\Controller;

class Player extends BaseController
{

    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PlayerModel();
        $data ['player'] = $model->getPlayer();
        echo view('player/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PlayerModel();
        $data ['player'] = $model->getPlayer($id);
        $model = new TeamModel();
        $data ['team'] = $model->getTeam();
        echo view('player/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $model = new TeamModel();
        $data ['team'] = $model->getTeam();
        $data ['validation'] = \Config\Services::validation();
        echo view('player/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'id_team'  => 'required',
                'amplua'  => 'required',
            ]))
        {
            $model = new PlayerModel();
            $model->save([
                'name' => $this->request->getPost('name'),
                'id_team' => $this->request->getPost('id_team'),
                'amplua' => $this->request->getPost('amplua'),
            ]);
            session()->setFlashdata('message', lang('Игрок был успешно добавлен!'));
            return redirect()->to('/player');
        }
        else
        {
            return redirect()->to('/player/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PlayerModel();
        
        helper(['form']);
        $data ['player'] = $model->getPlayer($id);
        $model = new TeamModel();
        $data ['team'] = $model->getTeam();
        $data ['validation'] = \Config\Services::validation();
        echo view('player/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/player/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'id_team'  => 'required',
                'amplua'  => 'required',
            ]))
        {
            $model = new PlayerModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'id_team' => $this->request->getPost('id_team'),
                'amplua' => $this->request->getPost('amplua'),
            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));
            return redirect()->to('/player');
        }
        else
        {
            return redirect()->to('/player/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PlayerModel();
        $model->delete($id);
        return redirect()->to('/player');
    }
}
