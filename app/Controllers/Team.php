<?php namespace App\Controllers;

use App\Models\TeamModel;
use CodeIgniter\Controller;

class Team extends BaseController
{

    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();
        $data ['team'] = $model->getTeam();
        echo view('team/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();
        $data ['team'] = $model->getTeam($id);
        echo view('team/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('team/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',

            ]))
        {
            $model = new TeamModel();
            $model->save([
                'name' => $this->request->getPost('name'),

            ]);
            session()->setFlashdata('message', lang('Команда успешно добавлена!'));
            return redirect()->to('/team');
        }
        else
        {
            return redirect()->to('/team/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();

        helper(['form']);
        $data ['team'] = $model->getTeam($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('team/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        echo '/team/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[255]',

            ]))
        {
            $model = new TeamModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),

            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/team');
        }
        else
        {
            return redirect()->to('/team/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TeamModel();
        $model->delete($id);
        return redirect()->to('/team');
    }
}
