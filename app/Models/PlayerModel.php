<?php namespace App\Models;
use CodeIgniter\Model;
class PlayerModel extends Model
{
    protected $table = 'player'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'id_team', 'name', 'amplua'];
    public function getPlayer($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
