<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('player/update'); ?>
    <input type="hidden" name="id" value="<?= $player["id"] ?>">

    <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= $player["name"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Команда</label>
        <select class="form-control" name="id_team">
            <?php
            $a=$player["id_team"]-1;
            echo "<option value=".$team[$a]["id"].">".$team[$a]["name"]."</option>";
            foreach ($team as $row)
            {
               if($player["id_team"]!=$row["id"])
               { 
               echo "<option value=".$row["id"].">".$row["name"]."</option>";
               } 
            }
            ?>
        </select>         

        <div class="invalid-feedback">
            <?= $validation->getError('id_team') ?>
        </div>

    </div>
    <div class="form-group">
        <label class="form-check-label">Амплуа</label>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="amplua" value="Защитник" <?= $player["amplua"]=='Защитник' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Защитник</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="amplua" value="Полузащитник" <?= $player["amplua"]=='Полузащитник' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Полузащитник</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="amplua" value="Нападающий" <?= $player["amplua"]=='Нападающий' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Нападающий</small>
            </label>
        </div>
        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('amplua') ?>
        </div>
    </div>

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
    </div>
<?= $this->endSection() ?>
