<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($team)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                    <?php if ($team['id'] == 1) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t3.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 2) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t7.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 3) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t90.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 4) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t36.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 5) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t35.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 6) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t21.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 7) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t39.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 8) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t11.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 9) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t31.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 10) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t13.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 11) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t14.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 12) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t2.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 13) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t43.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 14) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t1.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 15) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t4.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 16) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t20.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 17) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t6.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 18) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t54.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 19) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t8.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php elseif ($team['id'] == 20) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t49.svg" class="card-img" alt="<?= esc($team['name']); ?>">
                    <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($team['name']); ?></h5>
                    <p class="card-text">
                    <?php if ($team['id'] == 1) : ?>
                    Лондон, Emirates Stadium
                    <?php elseif ($team['id'] == 2) : ?>
                    Бирмингем, Villa Park
                    <?php elseif ($team['id'] == 3) : ?>
                    Бернли, Turf Moor
                    <?php elseif ($team['id'] == 4) : ?>
                    Брайтон, Amex Stadium
                    <?php elseif ($team['id'] == 5) : ?>
                    Уэст-Бромидж, The Hawtorns
                    <?php elseif ($team['id'] == 6) : ?>
                    Лондон, London Stadium
                    <?php elseif ($team['id'] == 7) : ?>
                    Вулвергемптон, Molineux Stadium
                    <?php elseif ($team['id'] == 8) : ?>
                    Ливерпуль, Villa Park
                    <?php elseif ($team['id'] == 9) : ?>
                    Лондон, Goodison Park
                    <?php elseif ($team['id'] == 10) : ?>
                    Лестер, King Power Stadium
                    <?php elseif ($team['id'] == 11) : ?>
                    Ливерпуль, Anfield
                    <?php elseif ($team['id'] == 12) : ?>
                    Лидс, Elland Road
                    <?php elseif ($team['id'] == 13) : ?>
                    Манчестер, Etihad Stadium
                    <?php elseif ($team['id'] == 14) : ?>
                    Манчестер, Old Trafford
                    <?php elseif ($team['id'] == 15) : ?>
                    Ньюкасл-апон-Тайн, St. James' Park
                    <?php elseif ($team['id'] == 16) : ?>
                    Саутгемптон, St. Mary's Park
                    <?php elseif ($team['id'] == 17) : ?>
                    Лондон, Tottenham Hotspur Stadium
                    <?php elseif ($team['id'] == 18) : ?>
                    Лондон, Craven Cottage
                    <?php elseif ($team['id'] == 19) : ?>
                    Лондон, Stamford Bridge
                    <?php elseif ($team['id'] == 20) : ?>
                    Шеффилд, Bramall Lane
                    <?php endif ?>
                    </p>
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
