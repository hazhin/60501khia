<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<h2>Все команды</h2>

<?php if (!empty($team) && is_array($team)) : ?>

    <?php foreach ($team as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if ($item['id'] == 1) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t3.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 2) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t7.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 3) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t90.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 4) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t36.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 5) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t35.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 6) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t21.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 7) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t39.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 8) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t11.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 9) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t31.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 10) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t13.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 11) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t14.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 12) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t2.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 13) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t43.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 14) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t1.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 15) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t4.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 16) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t20.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 17) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t6.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 18) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t54.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 19) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t8.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php elseif ($item['id'] == 20) : ?>
                    <img height="150" src="https://resources.premierleague.com/premierleague/badges/t49.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php endif ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        <a href="<?= base_url()?>/team/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>

<?php else : ?>

    <p>Невозможно найти рейтинги.</p>

<?php endif ?>
</div>
<?= $this->endSection() ?>
