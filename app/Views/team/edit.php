<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('team/update'); ?>
    <input type="hidden" name="id" value="<?= $team["id"] ?>">

    <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= $team["name"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>

    </div>

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
    </div>
<?= $this->endSection() ?>
